#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <math.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    lock_hexadecimal_button();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::lock_hexadecimal_button() {
    ui->pushButton_A->setDisabled(true);
    ui->pushButton_B->setDisabled(true);
    ui->pushButton_C->setDisabled(true);
    ui->pushButton_D->setDisabled(true);
    ui->pushButton_E->setDisabled(true);
    ui->pushButton_F->setDisabled(true);
}

void MainWindow::unlock_hexadecimal_button() {
    ui->pushButton_A->setDisabled(false);
    ui->pushButton_B->setDisabled(false);
    ui->pushButton_C->setDisabled(false);
    ui->pushButton_D->setDisabled(false);
    ui->pushButton_E->setDisabled(false);
    ui->pushButton_F->setDisabled(false);
}

int CURRENT_BASE = 10;
const unsigned int DEC = 10, HEX = 16, OCT = 8, BIN = 2;


QString base10_to_baseX(int base10_value, const int baseX);
void reverse(QString& str);



void reverse(QString& str) {
    const int STR_SIZE = str.size();
    for(int x = 0; x < floor(STR_SIZE / 2); x++) {
        QChar temp = str[x];
        str[x] = str[(STR_SIZE - 1) - x];
        str[(STR_SIZE - 1) - x] = temp;
    }
}




QString base10_to_baseX(int base10_value, const int baseX) {
    const QString encode_chars = "0123456789ABCDEF";
    QString encoded_string_value;
    while(base10_value > 0) {
        encoded_string_value.append(encode_chars[base10_value % baseX]);
        base10_value /=  baseX;
    }
    return encoded_string_value;
}

int baseX_to_base10(int baseX, QString& baseX_value) {
    int x = baseX_value.size() - 1, j = 0, base10_value = 0;

    if(baseX == DEC)
        return baseX_value.toInt();


    while(x >= 0) {

         if(baseX_value[x] >= 'A' && baseX_value[x] <= 'F')
            base10_value += (baseX_value[x].toLatin1() - 55) * pow(baseX, j);
         else
            base10_value += (baseX_value[x].toLatin1() - '0') * pow(baseX, j);
         ++j;
         --x;
    }
    return base10_value;
}

QString baseX_to_baseX(int baseX1, int baseX2, QString& value_str) {

    int value_int = 0;
    if(baseX1 == DEC) {
        value_int = value_str.toInt();

    } else {
        value_int = baseX_to_base10(baseX1, value_str);
    }

    QString result = base10_to_baseX(value_int, baseX2);
    reverse(result);
    return result;
}


void MainWindow::on_pushButton_Delete_clicked()
{
    QString input_label_text = ui->input_label->text();
    QString new_input_label_text;
    for(int i = 0; i < (input_label_text.size() - 1); i++) {
        new_input_label_text.append(input_label_text[i]);
    }
    ui->input_label->setText(new_input_label_text);
}

void MainWindow::on_pushButton_0_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_0->text());
}

void MainWindow::on_pushButton_1_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_1->text());
}



void MainWindow::on_pushButton_2_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_2->text());
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_3->text());
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_4->text());
}

void MainWindow::on_pushButton_5_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_5->text());
}

void MainWindow::on_pushButton_6_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_6->text());
}

void MainWindow::on_pushButton_7_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_7->text());
}

void MainWindow::on_pushButton_8_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_8->text());
}

void MainWindow::on_pushButton_9_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_9->text());
}

void MainWindow::on_pushButton_A_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_A->text());
}


void MainWindow::on_pushButton_B_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_B->text());
}

void MainWindow::on_pushButton_C_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_C->text());
}

void MainWindow::on_pushButton_D_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_D->text());
}

void MainWindow::on_pushButton_F_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_F->text());
}

void MainWindow::on_pushButton_E_clicked()
{
    ui->input_label->setText(ui->input_label->text() + ui->pushButton_E->text());
}

void MainWindow::on_pushButton_toHex_clicked()
{
    if(CURRENT_BASE == HEX) {
        ui->result_label->setText(ui->input_label->text() + "(16)");
        return;
    }
    QString input_label_text = ui->input_label->text();
    QString result = baseX_to_baseX(CURRENT_BASE, HEX, input_label_text);
    ui->result_label->setText(result + "(16)");
}


void MainWindow::on_pushButton_toDec_clicked()
{
    if(CURRENT_BASE == DEC) {
        ui->result_label->setText(ui->input_label->text() + "(10)");
        return;
    }

    QString input_label_text = ui->input_label->text();
    QString result = baseX_to_baseX(CURRENT_BASE, DEC, input_label_text);
    ui->result_label->setText(result + "(10)");
}

void MainWindow::on_pushButton_toBin_clicked()
{
    if(CURRENT_BASE == BIN) {
        ui->result_label->setText(ui->input_label->text() + "(2)");
        return;
    }
    QString input_label_text = ui->input_label->text();
    QString result = baseX_to_baseX(CURRENT_BASE, BIN, input_label_text);
    ui->result_label->setText(result + "(2)");

}

void MainWindow::on_pushButton_toOct_clicked()
{
    if(CURRENT_BASE == OCT) {
        ui->result_label->setText(ui->input_label->text() + "(8)");
        return;
    }
    QString input_label_text = ui->input_label->text();
    QString result = baseX_to_baseX(CURRENT_BASE, OCT, input_label_text);
    ui->result_label->setText(result + "(8)");
}

void MainWindow::on_pushButton_inHex_clicked()
{
    CURRENT_BASE = 16;
    ui->current_base_label->setText("BASE: 16");
    unlock_hexadecimal_button();
}

void MainWindow::on_pushButton_inDec_clicked()
{
    CURRENT_BASE = 10;
    ui->current_base_label->setText("BASE: 10");
    lock_hexadecimal_button();
}

void MainWindow::on_pushButton_inOct_clicked()
{
    CURRENT_BASE = 8;
    ui->current_base_label->setText("BASE: 8");
    lock_hexadecimal_button();
}

void MainWindow::on_pushButton_inBin_clicked()
{
    CURRENT_BASE = 2;
    ui->current_base_label->setText("BASE: 2");
    lock_hexadecimal_button();
}


