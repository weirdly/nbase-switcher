#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:
    void lock_hexadecimal_button();
    void unlock_hexadecimal_button();
private slots:
    void on_pushButton_0_clicked();

    void on_pushButton_1_clicked();

    void on_pushButton_Delete_clicked();


    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_pushButton_toHex_clicked();

    void on_pushButton_A_clicked();

    void on_pushButton_B_clicked();

    void on_pushButton_C_clicked();

    void on_pushButton_D_clicked();

    void on_pushButton_F_clicked();

    void on_pushButton_E_clicked();

    void on_pushButton_toDec_clicked();

    void on_pushButton_inHex_clicked();

    void on_pushButton_inDec_clicked();

    void on_pushButton_inOct_clicked();

    void on_pushButton_inBin_clicked();

    void on_pushButton_toBin_clicked();

    void on_pushButton_toOct_clicked();

private:
    Ui::MainWindow *ui;
};



#endif // MAINWINDOW_H
